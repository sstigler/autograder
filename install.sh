#!/bin/bash
# vim: ft=sh:tw=80:ts=4:sta:sw=4:si:ci
#************************************************************** SHELL SCRIPT ***
#   NAME
#       install.sh -- Install the programm
#
#
#   FIRST RELEASE
#       2015-06-11  Sebastian Stigler		sebastian.stigler@hs-aalen.de
#
#   COPYRIGHT (C) 2015
#*******************************************************************************
#** declaration

SRC_DIR=`pwd`
DST_DIR=/usr/local/bin/
FILE=autograder.py

ln -sf ${SRC_DIR}/${FILE} ${DST_DIR}/${FILE}

#*********************************************************************** END ***
